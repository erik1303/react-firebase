// Scripts for firebase and firebase messaging
// eslint-disable-next-line no-undef
importScripts("https://www.gstatic.com/firebasejs/8.2.0/firebase-app.js");
// eslint-disable-next-line no-undef
importScripts("https://www.gstatic.com/firebasejs/8.2.0/firebase-messaging.js");

// Initialize the Firebase app in the service worker by passing the generated config
const firebaseConfig ={ 
  apiKey:"AIzaSyDmjFwi0z3x03snLE0mR5Q5qkaPC8nG0YM", 
  authDomain:"helloworld-bcce7.firebaseapp.com", 
  projectId:"helloworld-bcce7", 
  storageBucket:"helloworld-bcce7.appspot.com", 
  messagingSenderId:"747732874015", 
  appId:"1:747732874015:web:e76629a7fcdd8bcaa3f62a", 
  measurementId:"G-P34LKJ0J8X" 
};

// eslint-disable-next-line no-undef
firebase.initializeApp(firebaseConfig);

// Retrieve firebase messaging
// eslint-disable-next-line no-undef
const messaging = firebase.messaging();

messaging.onBackgroundMessage(function (payload) {
  console.log("Received background message ", payload);

  const notificationTitle = payload.notification.title;
  const notificationOptions = {
    body: payload.notification.body,
    icon: "/logo192.png",
  };

  // eslint-disable-next-line no-restricted-globals
  return self.registration.showNotification(
    notificationTitle,
    notificationOptions
  );
});
