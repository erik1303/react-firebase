import firebase from "firebase/app";
import "firebase/messaging";

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig ={ 
  apiKey:"AIzaSyDmjFwi0z3x03snLE0mR5Q5qkaPC8nG0YM", 
  authDomain:"helloworld-bcce7.firebaseapp.com", 
  projectId:"helloworld-bcce7", 
  storageBucket:"helloworld-bcce7.appspot.com", 
  messagingSenderId:"747732874015", 
  appId:"1:747732874015:web:e76629a7fcdd8bcaa3f62a", 
  measurementId:"G-P34LKJ0J8X" 
};

firebase.initializeApp(firebaseConfig);

const messaging = firebase.messaging();

const { REACT_APP_VAPID_KEY } = process.env;
const publicKey = REACT_APP_VAPID_KEY;

export const getToken = async (setTokenFound) => {
  let currentToken = "";

  try {
    currentToken = await messaging.getToken({ vapidKey: publicKey });
    if (currentToken) {
      setTokenFound(true);
    } else {
      setTokenFound(false);
    }
  } catch (error) {
    console.log("An error occurred while retrieving token. ", error);
  }

  return currentToken;
};

export const onMessageListener = () =>
  new Promise((resolve) => {
    messaging.onMessage((payload) => {
      resolve(payload);
    });
  });
